<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Destination;

class DestinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Destination::create([
            'name' => 'Место 1',
            'long' => 53.12345,
            'lat' => 27.54321,
        ]);

        Destination::create([
            'name' => 'Место 2',
            'long' => 52.98765,
            'lat' => 28.13579,
        ]);

        Destination::create([
            'name' => 'Место 3',
            'long' => 52.98765,
            'lat' => 28.13579,
        ]);

        Destination::create([
            'name' => 'Место 4',
            'long' => 52.98765,
            'lat' => 28.13579,
        ]);

    }
}
